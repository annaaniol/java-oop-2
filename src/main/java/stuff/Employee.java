package stuff;
public class Employee {

    String name;
    private int salary;

    public Employee(String name, int salary){
        this.name=name;
        this.salary=salary;
    }

    public String getName(){
        return name;
    }
    public int getSalary(){
        return salary;
    }
    public boolean ifSatisfied(){
        return salary > 10000;
    }

    @Override
    public String toString() {
        return name + " - Employee \n";
    }
}
