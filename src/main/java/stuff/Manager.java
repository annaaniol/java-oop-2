package stuff;
import java.util.ArrayList;
import java.util.List;

public class Manager extends Employee {

    public List<Employee> employeeList = new ArrayList<Employee>();

    Manager(String name, int salary) {
        super(name, salary);
    }
    public boolean ifCanHire(Employee candidate) {
        return true;
    }
    public void hire(Employee candidate){
        if(this.ifCanHire(candidate)){
            employeeList.add(candidate);
        }
    }
    int countSalaries(){
        int sum=0;
        for(Employee e : employeeList){
            sum += e.getSalary();
        }
        return sum;
    }
    int countEmployees(){
        return employeeList.size();
    }

    @Override
    public String toString(){
        String hierarchy = this.name+" - Manager\n";
        for(Employee employee : employeeList){
            hierarchy += "\t\t"+employee.getName()+" - Employee\n";
        }
        return hierarchy;
    }
}
