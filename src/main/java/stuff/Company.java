package stuff;

public class Company {
    private static final Company Instance = new Company();
    private CEO companyCEO;

    private Company(){}

    public static Company getCompany() {
        return Instance;
    }
    public void hireCEO(CEO companyCEO){
        this.companyCEO = companyCEO;
    }

    @Override
    public String toString(){
        return companyCEO.toString();
    }
}
