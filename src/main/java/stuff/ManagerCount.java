package stuff;
public class ManagerCount extends Manager {

    private int maxEmployeeAmount;

    public ManagerCount(String name, int salary, int maxEmployeeAmount) {
        super(name, salary);
        this.maxEmployeeAmount=maxEmployeeAmount;
    }

    @Override
    public boolean ifCanHire(Employee candidate){
        return this.countEmployees() < maxEmployeeAmount;
    }
}
