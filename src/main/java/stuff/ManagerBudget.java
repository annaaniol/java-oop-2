package stuff;

public class ManagerBudget extends Manager {

    private int budget;

    public ManagerBudget(String name, int salary, int budget) {
        super(name, salary);
        this.budget=budget;
    }

    @Override
    public boolean ifCanHire(Employee candidate){
        return candidate.getSalary() <= budget - this.countSalaries();
    }
}
