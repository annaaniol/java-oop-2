package stuff;

public class CEO extends Manager {
    public CEO(String name, int salary) {
        super(name, salary);
    }

    @Override
    public String toString(){
        String hierarchy = this.getName() + " - CEO\n";
        for(Employee employee: this.employeeList) {
            hierarchy += "\t" + employee.toString();
        }
        return hierarchy;
    }
}