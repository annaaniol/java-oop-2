package runner;
import stuff.*;

public class Runner {
    public static void main(String[] args){

        CEO companyCEO = new CEO("Grzegorz", 110000);

        ManagerCount managerC = new ManagerCount("Marcin", 75000, 5);
        ManagerBudget managerB = new ManagerBudget("Stanisław", 80000, 300000);

        Employee employee1 = new Employee("Kamil", 50000);
        Employee employee2 = new Employee("Mariusz", 50000);
        Employee employee3 = new Employee("Gerwazy", 50000);
        Employee employee4 = new Employee("Karol", 50000);
        Employee employee5 = new Employee("Maciej", 50000);

        Company FunnyCompany = Company.getCompany();

        FunnyCompany.hireCEO(companyCEO);
        companyCEO.hire(managerB);
        companyCEO.hire(managerC);
        managerB.hire(employee1);
        managerB.hire(employee2);
        managerB.hire(employee3);
        managerC.hire(employee4);
        managerC.hire(employee5);

        System.out.print(FunnyCompany.toString());
    }
}
