import org.junit.Before;
import org.junit.Test;
import stuff.*;

import static org.junit.Assert.assertEquals;

public class ToStringTest {

    private Company FunnyCompany = Company.getCompany();
    private CEO companyCEO = new CEO("Grzegorz", 110000);
    private ManagerCount managerC = new ManagerCount("Marcin", 75000, 5);
    private Employee employee1 = new Employee("Kamil", 50000);
    private Employee employee2 = new Employee("Mariusz", 50000);

    @Before
    public void setUp(){
        FunnyCompany.hireCEO(companyCEO);
        companyCEO.hire(managerC);
        managerC.hire(employee1);
        managerC.hire(employee2);
    }

    @Test
    public void testToString(){
        assertEquals("Cannot print hierarchy of the company","Grzegorz - CEO\n\tMarcin - Manager\n\t\tKamil - Employee\n\t\tMariusz - Employee\n", FunnyCompany.toString());
    }
}
