import stuff.*;

import org.junit.Before;
import org.junit.Test;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class ManagerTest {
    private List<Employee> expectedBudgetEmployees = new ArrayList<Employee>();
    private List<Employee> expectedCountEmployees = new ArrayList<Employee>();

    private ManagerBudget managerB = new ManagerBudget("Grzegorz", 110000, 150000);
    private ManagerCount managerC = new ManagerCount("Marcin", 55000, 3);

    private Employee employee1 = new Employee("Kamil", 50000);
    private Employee employee2 = new Employee("Mariusz", 50000);
    private Employee employee3 = new Employee("Ramzes", 50000);
    private Employee employee4 = new Employee("Karol", 50000);
    private Employee employee5 = new Employee("Maciej", 50000);

    @Before
    public void setUp(){

        expectedBudgetEmployees.add(employee1);
        expectedBudgetEmployees.add(employee2);
        expectedBudgetEmployees.add(employee3);

        expectedCountEmployees.add(employee1);
        expectedCountEmployees.add(employee2);
        expectedCountEmployees.add(employee3);

    }
    @Test
    public void testBudgetHire(){
        managerB.hire(employee1);
        managerB.hire(employee2);
        managerB.hire(employee3);
        managerB.hire(employee4);
        managerB.hire(employee5);
        assertThat("Unexpected result of hiring (BudgetManager case)",managerB.employeeList, is(expectedBudgetEmployees));
    }
    @Test
    public void testCountHire(){
        managerC.hire(employee1);
        managerC.hire(employee2);
        managerC.hire(employee3);
        managerC.hire(employee4);
        managerC.hire(employee5);
        assertThat("Unexpected result of hiring (CountManager case)",managerC.employeeList, is(expectedCountEmployees));
    }
}
