import org.junit.Test;
import stuff.Employee;

import static org.junit.Assert.*;
public class EmployeeTest {

    private Employee employee1 = new Employee("Jan", 5000);
    private Employee employee2 = new Employee("Tadeusz", 1000000);

    @Test
    public void testName(){
        assertEquals("Cannot answer the question about name", employee1.getName(), "Jan");
    }
    @Test
    public void testSalary(){
        assertEquals("Cannot answer the question about salary", employee1.getSalary(), 5000);
    }
    @Test
    public void testSatisfaction(){
        assertFalse("Employee with 5000 salary should be unsatisfied", employee1.ifSatisfied());
        assertTrue("Employee with 1000000 salary should be satisfied", employee2.ifSatisfied());
    }

}
